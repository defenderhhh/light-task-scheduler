package com.lts.core.domain;

/**
 * Created by hugui.hg on 4/7/16.
 */
public enum JobType {

    REAL_TIME,
    TRIGGER_TIME,
    CRON,
    REPEAT

}
